<?php
include "Animal.php";
require_once("Frog.php");
require_once("Ape.php");
$sheep = new Animal("shaun");
echo $sheep->name; // "shaun" 
echo "<br>";
echo $sheep->legs; // 4 
echo "<br>";
echo $sheep->cold_blooded; // "no" 
echo "<br>";
$sungokong = new Ape("kera sakti"); 
$sungokong->yell(); // "Auooo" 
echo "<br>";
$kodok = new Frog("buduk"); 
$kodok->jump(); // "hop hop" 
echo "<br>";

echo "Output Akhir:";
echo "<br><br>";
echo "Name: $sheep->name<br>";
echo "legs: $sheep->legs<br>";
echo "cold blooded: $sheep->cold_blooded";

echo "<br><br>";
echo "Name: $kodok->name<br>";
echo "legs: $kodok->legs<br>";
echo "cold blooded: $kodok->cold_blooded<br>";
echo "jump : ";
$kodok->jump();

echo "<br><br>";
echo "Name: $sungokong->name<br>";
echo "legs: $sungokong->legs<br>";
echo "cold blooded: $sungokong->cold_blooded<br>";
echo ("yell : ");
$sungokong->yell();
?>